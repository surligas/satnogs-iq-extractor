/*
 * extractor.h
 *
 *  Created on: Apr 4, 2021
 *      Author: surligas
 */

#ifndef EXTRACTOR_H_
#define EXTRACTOR_H_

#include <string>
#include <memory>

class extractor
{
public:
  typedef std::shared_ptr<extractor> sptr;

  /**
   * Output format
   */
  typedef enum {
    INVALID,/**< Invalid format */
    RAW,    /**< Raw binary */
    HDF5    /**< HDF5 */
  } output_t;

  extractor (const std::string& in_dir, const std::string& out_dir,
             size_t block_size, size_t skip_start, size_t skip_end,
             bool awgn, size_t awgn_blocks, const std::string &file_extension);
  virtual
  ~extractor ();

  virtual void
  print_settings();

  bool
  has_signal(uint64_t obsid);

  virtual void
  extract() = 0;

  const std::string&
  file_extension() const;

protected:
  const std::string m_input_dir;
  const std::string m_output_dir;
  const size_t m_block_size;
  const size_t m_skip_start;
  const size_t m_skip_end;
  const bool m_awgn;
  const size_t m_awgn_blocks;
private:
  const std::string m_file_extension;
};

#endif /* EXTRACTOR_H_ */
