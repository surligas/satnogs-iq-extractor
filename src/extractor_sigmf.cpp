/*
 * extractor_sigmf.cpp
 *
 *  Created on: Apr 4, 2021
 *      Author: surligas
 */

#include "extractor_sigmf.h"
#include "sigmf/meta_container.h"

#include <iostream>
#include <archive.h>
#include <archive_entry.h>
#include <fcntl.h>
#include <exception>
#include <cstring>
#include <fstream>
#include <random>


const size_t extractor_sigmf::read_buffer_len = 1024;

extractor_sigmf::extractor_sigmf (const std::string &in_dir,
                                  const std::string &out_dir, size_t block_size,
                                  size_t skip_start, size_t skip_end,
                                  bool awgn, size_t awgn_blocks) :
        extractor (in_dir, out_dir, block_size, skip_start, skip_end,
                   awgn, awgn_blocks, ".sigmf"),
        /* FIXME! hardcoded sc16 support detected!!!! */
        m_sizeof_sample(2 * sizeof(int16_t))
{
}

extractor_sigmf::~extractor_sigmf ()
{
}

void
extractor_sigmf::print_settings ()
{
  std::cout << "Metadata Engine           : " << "SigMF" << std::endl;
  std::cout << "Output Format             : " << "Raw" << std::endl;
  extractor::print_settings ();
}

void
extractor_sigmf::extract ()
{
  std::cout << "Scanning for SigMF IQ captures inside the directory "
      << m_input_dir << std::endl;
  for (auto p : std::filesystem::recursive_directory_iterator (m_input_dir)) {
    /* Check for all .tar files containing sigmf metadata*/
    if (p.path ().extension () == ".tar") {
      std::cout << p.path() << "              : ";
      try {
        sigmf::meta_container c(p.path().string());
        extract_single(p.path(), c.json());
        std::cout << "OK" << std::endl;
      }
      catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
      }
    }
  }
}

void
extractor_sigmf::extract_single (const std::filesystem::path &tar,
                                 const nlohmann::json &j)
{
  /* First output dir level is the sampling rate */
  const double samp_rate(j["global"]["core:sample_rate"].get<double>());
  std::filesystem::path p(m_output_dir);
  p /= std::to_string(samp_rate);
  std::filesystem::create_directories(p);

  double resampling_ratio = j["global"]["satnogs:decoder_resampling_ratio"];
  uint64_t phase = j["global"]["satnogs:decoder_phase"];

  struct archive *tar_arch = archive_read_new ();
  if (!tar_arch) {
    throw std::runtime_error (
        "Could not allocate reading achieve memory structure");
  }

  if(archive_read_support_format_tar(tar_arch)) {
      throw std::runtime_error (
        "Could set the tar filter");
  }

  struct archive_entry *entry;
  int r;
  r = archive_read_open_filename(tar_arch, tar.c_str(), 10240);
  bool found_data = false;
  while ((r = archive_read_next_header (tar_arch, &entry)) == ARCHIVE_OK) {
    std::string s (archive_entry_pathname (entry));
    found_data = s.size () >= sigmf::meta_container::data_extension.size ()
        && std::equal (sigmf::meta_container::data_extension.rbegin (),
                       sigmf::meta_container::data_extension.rend (),
                       s.rbegin ());
    if(found_data) {
      break;
    }
  }

  if(!found_data) {
    return;
  }

  /*
   * Annotations contains no decoded frames. Perhaps this observation can
   * be used to retrieve AWGN samples
   */
  if(j["annotations"].size() == 0) {
    extract_awgn(tar_arch, j["global"]["satnogs:observation_id"], samp_rate);
    archive_read_close(tar_arch);
    archive_read_free(tar_arch);
    return;
  }

  uint64_t index = 0;
  for(auto& elem : j["annotations"]) {
    uint64_t sample_cnt = elem["core:sample_count"];
    uint64_t sample_start = elem["core:sample_start"];
    double timing_error = elem["satnogs:symbol_timing_error"];

    /*
     * Correct the sample start based on the timing error, the phase and the
     * resampling ratio.
     *
     * Note that the the phase offset does not need any kind of correction
     */
    uint64_t true_sample_start = resampling_ratio
        * (sample_start * timing_error) - phase + m_skip_start;
    uint64_t true_sample_cnt = resampling_ratio * (sample_cnt * timing_error)
        - m_skip_end;


    char buf[m_block_size * m_sizeof_sample];

    /* Seek until the start of the frame */
    while(index < true_sample_start * m_sizeof_sample) {
      uint64_t block = std::min(true_sample_start * m_sizeof_sample,
                                index + m_block_size * m_sizeof_sample) - index;
      ssize_t ret = archive_read_data(tar_arch, buf, block);
      if(ret <= 0) {
        archive_read_close(tar_arch);
        archive_read_free(tar_arch);
        throw std::runtime_error("unexpected end of file");
      }
      index += ret;
    }

    for(uint64_t i = 0; i < true_sample_cnt / m_block_size; i++) {
      ssize_t ret = archive_read_data(tar_arch, buf, m_block_size * m_sizeof_sample);
      if(ret != m_block_size * m_sizeof_sample) {
        throw std::runtime_error("could not read the specified block size");
      }
      const std::string fname(elem["satnogs:time"].get<std::string>() + "-" + std::to_string(i));
      const std::filesystem::path outp(p / fname );
      std::ofstream wf(outp.string(), std::ios::out | std::ios::binary);
      wf.write(buf, m_block_size * m_sizeof_sample);
      index += m_block_size * m_sizeof_sample;
    }
  }
}

void
extractor_sigmf::extract_awgn (struct archive *tar_arch,
                               uint64_t obsid, double samp_rate)
{
  if (has_signal(obsid)) {
    return;
  }
  /* First output dir level is the sampling rate */
  std::filesystem::path p (m_output_dir);
  p /= std::to_string (samp_rate);
  /* Second output dir level is the awgn class */
  p /= "awgn";
  std::filesystem::create_directories (p);

  auto gen = std::bind (std::uniform_int_distribution<> (0, 1),
                        std::default_random_engine ());
  for (size_t i = 0; i < m_awgn_blocks;) {
    char buf[m_block_size * m_sizeof_sample];
    ssize_t ret = archive_read_data (tar_arch, buf,
                                     m_block_size * m_sizeof_sample);
    if (ret != m_block_size * m_sizeof_sample) {
      return;
    }
    if (gen ()) {
      const std::string fname (
          std::to_string (obsid) + "-" + std::to_string (i));
      const std::filesystem::path outp (p / fname);
      std::ofstream wf (outp.string (), std::ios::out | std::ios::binary);
      wf.write (buf, m_block_size * m_sizeof_sample);
      i++;
    }
  }
}

