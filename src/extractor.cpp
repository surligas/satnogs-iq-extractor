/*
 * extractor.cpp
 *
 *  Created on: Apr 4, 2021
 *      Author: surligas
 */

#include "extractor.h"
#include <filesystem>
#include <iostream>


#include <cpprest/http_client.h>
#include <cpprest/http_client.h>
#include <cpprest/filestream.h>
#include <cpprest/uri.h>
#include <cpprest/json.h>

using namespace utility;
using namespace web;
using namespace web::http;
using namespace web::http::client;
using namespace concurrency::streams;

extractor::extractor (const std::string &in_dir, const std::string &out_dir,
                      size_t block_size, size_t skip_start, size_t skip_end,
                      bool awgn,
                      size_t awgn_blocks,
                      const std::string &file_extension) :
        m_input_dir (in_dir),
        m_output_dir (out_dir),
        m_block_size (block_size),
        m_skip_start (skip_start),
        m_skip_end (skip_end),
        m_awgn(awgn),
        m_awgn_blocks (awgn_blocks),
        m_file_extension(file_extension)
{
  if (!std::filesystem::is_directory (in_dir)) {
    throw std::invalid_argument (in_dir + " is not a directory");
  }

  if (!std::filesystem::is_directory (out_dir)) {
    throw std::invalid_argument (out_dir + " is not a directory");
  }
}

extractor::~extractor ()
{
}

void
extractor::print_settings ()
{
  std::cout << "Input directory           : " << m_input_dir << std::endl;
  std::cout << "Output directory          : " << m_output_dir << std::endl;
  std::cout << "Block Size                : " << m_block_size << std::endl;
  std::cout << "Frame Start samples skip  : " << m_skip_start << std::endl;
  std::cout << "Frame End samples skip    : " << m_skip_end << std::endl;
  std::cout << "Create AWGN blocks        : " << (m_awgn ? "Yes" : "No") << std::endl;
  std::cout << "Number of AWGN blocks     : " << m_awgn_blocks << std::endl;
}

const std::string&
extractor::file_extension () const
{
  return m_file_extension;
}

static void
_has_signal(bool &result, uint64_t obsid)
{
  /* Query the SatNOGS network if this observation was reported with no signal */
      auto request_json = http_client (U("https://network.satnogs.org"))
          .request(methods::GET,
                   uri_builder(U("api")).append_path(U("observations"))
                               .append_query(U("id"), obsid).to_string())
          .then([&](http_response response) {
                        if (response.status_code() != 200) {
                                throw std::runtime_error("Returned " + std::to_string(response.status_code()));
                        }
                        return response.extract_json();
                })
                .then([&](json::value jsonObject) {
                        return jsonObject[0][U("waterfall_status")].as_string() == "without-signal";
                })
                .then([&](bool no_signal) {
                        result = !no_signal;
      });
      request_json.wait();
}


bool
extractor::has_signal (uint64_t obsid)
{
  bool signal;
  _has_signal(signal, obsid);
  return signal;
}
