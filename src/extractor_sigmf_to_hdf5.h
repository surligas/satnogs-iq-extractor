/*
 * extractor_sigmf_to_hdf5.h
 *
 *  Created on: May 26, 2021
 *      Author: surligas
 */

#include "extractor.h"
#include "sigmf/meta_container.h"
#include <H5Cpp.h>
#include <h5cpp/all>

#ifndef SRC_EXTRACTOR_SIGMF_TO_HDF5_H_
#define SRC_EXTRACTOR_SIGMF_TO_HDF5_H_

class extractor_sigmf_to_hdf5 : public extractor
{
public:
  static extractor::sptr
  make_shared (const std::string &in_dir, const std::string &out_dir,
               size_t block_size, size_t skip_start, size_t skip_end, bool awgn,
               size_t awgn_blocks);

  extractor_sigmf_to_hdf5 (const std::string& in_dir, const std::string& out_dir,
                           size_t block_size, size_t skip_start, size_t skip_end,
                           bool awgn, size_t awgn_blocks);
  ~extractor_sigmf_to_hdf5 ();

  void
  print_settings();

  void
  extract();

private:
  const size_t m_sizeof_data_sample;
  size_t m_max_class_samples;
  size_t m_class_samples_cnt;
  H5::H5File m_h5;

  void
  extract_samp_rate_group(double samp_rate,
                          const std::vector<sigmf::meta_container>& archs);

  void
  extract_class(size_t class_idx,
                size_t nclasses,
                const std::vector<sigmf::meta_container>& archs,
                h5::pt_t &iq_dataset,
                h5::pt_t &class_dataset,
                h5::pt_t &snr_dataset);

  void
  extract_awgn_class(size_t class_idx,
                     size_t nclasses,
                     const std::vector<sigmf::meta_container>& archs,
                     h5::pt_t &iq_dataset,
                     h5::pt_t &class_dataset,
                     h5::pt_t &snr_dataset);

  void
  extract_observation(size_t class_idx,
                      size_t nclasses,
                      const sigmf::meta_container& c,
                      h5::pt_t &iq_dataset,
                      h5::pt_t &class_dataset,
                      h5::pt_t &snr_dataset);

  void
  extract_awgn_observation(size_t class_idx,
                           size_t nclasses,
                           const sigmf::meta_container& c,
                           h5::pt_t &iq_dataset,
                           h5::pt_t &class_dataset,
                           h5::pt_t &snr_dataset,
                           size_t nblocks);

};

#endif /* SRC_EXTRACTOR_SIGMF_TO_HDF5_H_ */
