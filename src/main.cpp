/*
 * main.cpp
 *
 *  Created on: Apr 3, 2021
 *      Author: surligas
 */

#include "argagg.hpp"
#include "extractor.h"
#include "extractor_sigmf.h"
#include "extractor_sigmf_to_hdf5.h"
#include <iostream>

int
main (int argc, char **argv)
{
  argagg::parser argparser
    {
          {
            { "help",
              { "-h", "--help" }, "shows this help message", 0 },
            { "metaformat",
              { "-m", "--meta-format" },
                "Meta-format (Currently only sigmf is supported)."
                " Default is sigmf.", 1 },
            { "inputdir",
                  { "-i", "--input-dir" }, "input directory to scan for "
                      "RAW IQ data. Depends on the --meta-format paramters."
                      " If SigMF is used, the extractor handles all .tar files"
                      " inside the given directory that contain the raw IQ file"
                      " and the corresponding SigMF.",
                    1 },
            { "outputdir",
              { "-o", "--output-dir" }, "output directory for the extracted DB",
                1 },
            {"awgn", {"--awgn"},
             "if specified, the extractor will try to produce AWGN blocks from "
             "observations with no signal", 0},
            { "awgnblocks",
              { "--number-of-awgn-blocks" },
                "number of blocks with AWGN to produce. To do so, "
                    "the extractor queries the SatNOGS Network API and checks"
                    "which from the available observations have been reported"
                    "by users as bad. Meaning that they contain no observable "
                    "signal. By default the (maximum) number of the AWGN blocks that will"
                    " be produced is 1024. Requires the --awgn option", 1 },
                { "blocksize",
                  { "--block-size" },
                    "number of samples of each extracted block. The default is 1024"
                        " Each block is"
                        "guaranteed to have exactly this size. Frames or portion of"
                        "frames that are less of the block size are discarded.",
                    1 },
                { "skipstart",
                  { "--skip-start-samples" }, "Skip thespecified number of"
                    " samples at the start of the frame. Useful to bypass "
                    "preambles that may have a negative impact on the ML "
                    "system. The default is 256", 1

                },
                { "skipend",
                  { "--skip-end-samples" }, "Skip the specified number of"
                    " samples at the end of the frame. Useful to bypass "
                    "postabbles that may have a negative impact on the ML "
                    "system. The default is 256", 1
                },
                {
                    "outputformat",
                    {"--output-format"},
                    "Sets the output format of the dataset. Accepted values [raw, hdf5]."
                    " The default is 'raw'", 1
                },

          } };

  argagg::parser_results args;
  try {
    args = argparser.parse (argc, argv);
  }
  catch (const std::exception &e) {
    std::cerr << e.what () << std::endl;
    return EXIT_FAILURE;
  }

  if (args["help"]) {
    argagg::fmt_ostream fmt (std::cerr);
    fmt << argparser;
  }

  bool awgn = false;
  if (args["awgn"]) {
    awgn = true;
  }

  try {
    std::string meta = args["metaformat"].as<std::string>("sigmf");
    std::string inputdir = args["inputdir"].as<std::string>();
    std::string outputdir = args["outputdir"].as<std::string>();
    size_t n_awgn_blocks = args["awgnblocks"].as<size_t>(1024);
    size_t block_size = args["blocksize"].as<size_t>(1024);
    size_t skip_n_start = args["skipstart"].as<size_t>(256);
    size_t skip_n_end = args["skipend"].as<size_t>(256);
    extractor::output_t out_format = extractor::INVALID;

    if(args["outputformat"].as<std::string>() == "raw") {
      out_format = extractor::RAW;
    }
    else if(args["outputformat"].as<std::string>() == "hdf5") {
      out_format = extractor::HDF5;
    }

    extractor::sptr ex;

    switch(out_format) {
      case extractor::HDF5:
        ex = extractor_sigmf_to_hdf5::make_shared (inputdir, outputdir,
                                                   block_size, skip_n_start,
                                                   skip_n_end, awgn,
                                                   n_awgn_blocks);
        break;
      default:
        throw std::runtime_error("Unsupported extractor format");
    }

    ex->print_settings();
    ex->extract();
  }
  catch (const std::exception &e) {
    std::cerr << e.what () << std::endl;
    argagg::fmt_ostream fmt (std::cerr);
    fmt << argparser;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}

