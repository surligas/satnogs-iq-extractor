/*
 * extractor_sigmf.h
 *
 *  Created on: Apr 4, 2021
 *      Author: surligas
 */

#ifndef EXTRACTOR_SIGMF_H_
#define EXTRACTOR_SIGMF_H_

#include "extractor.h"
#include <filesystem>
#include <deque>
#include <nlohmann/json.hpp>

class extractor_sigmf : public extractor
{
public:
  static const size_t read_buffer_len;

  extractor_sigmf (const std::string &in_dir, const std::string &out_dir,
                   size_t block_size, size_t skip_start, size_t skip_end,
                   bool awgn, size_t awgn_blocks);
  virtual
  ~extractor_sigmf ();

  void
  print_settings();

  void
  extract();

private:
  const size_t m_sizeof_sample;
  std::deque<std::filesystem::directory_entry> d_files;

  void
  extract_single(const std::filesystem::path &tar, const nlohmann::json& j);

  void
  extract_awgn(struct archive *tar_arch, uint64_t obsid, double samp_rate);


};

#endif /* EXTRACTOR_SIGMF_H_ */
