/*
 * container.cpp
 *
 *  Created on: Apr 4, 2021
 *      Author: surligas
 */

#include <stdexcept>
#include "meta_container.h"
#include <iostream>
#include <vector>

namespace sigmf
{

//FIXME
//const std::string meta_container::meta_extension = ".sigmf-meta";
//const std::string meta_container::data_extension = ".sigmf-data";
const std::string meta_container::meta_extension = ".sigmf";
const std::string meta_container::data_extension = ".iq";

meta_container::meta_container (const std::string &filename)
: m_filename(filename),
  m_found_meta(false),
  m_found_data(false)
{
  struct archive *tar_arch = archive_read_new ();
  if (!tar_arch) {
    throw std::runtime_error (
        "Could not allocate reading achieve memory structure");
  }

  if(archive_read_support_format_tar(tar_arch)) {
      throw std::runtime_error (
        "Could set the tar filter");
  }

  struct archive_entry *entry;
  int r;
  r = archive_read_open_filename(tar_arch, m_filename.c_str(), 10240);
  while ((r = archive_read_next_header (tar_arch, &entry)) == ARCHIVE_OK) {
    std::string s (archive_entry_pathname (entry));
    bool found_meta = s.size () >= meta_extension.size ()
        && std::equal (meta_extension.rbegin (), meta_extension.rend (),
                       s.rbegin ());
    bool found_data = s.size () >= data_extension.size ()
        && std::equal (data_extension.rbegin (), data_extension.rend (),
                       s.rbegin ());

    if (found_data) {
      m_found_data = true;
    }
    /*
     * In case the entry is the SigMF file, try to parse it as JSON to avoid
     * open, closing again the tar stream, as libarchive does not support
     * entry based access
     */
    else if(found_meta) {
      m_found_meta = true;
      int64_t remaining = archive_entry_size(entry);
      uint8_t buff[256];
      std::vector<uint8_t> v;
      while(remaining) {
        ssize_t ret = archive_read_data(tar_arch, buff, sizeof(buff));
        if(ret <= 0) {
          break;
        }
        v.insert(v.end(), buff, buff + ret);
        remaining -= ret;
       }
      m_json = nlohmann::json::parse(v);
    }
  }
  if(!(m_found_meta && m_found_data)) {
    throw std::runtime_error (filename + " is not a valid SigMF archive");
  }

  if(!m_json.contains("global")) {
    throw std::runtime_error (filename + " does not contain SigMF global mandatory section");
  }

  archive_read_close(tar_arch);
  archive_read_free(tar_arch);
}

meta_container::~meta_container ()
{
}

const nlohmann::json&
meta_container::json () const
{
  return m_json;
}


const std::string&
meta_container::path() const
{
 return m_filename;
}

} /* namespace sigmf */
