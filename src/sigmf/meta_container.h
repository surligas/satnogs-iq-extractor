/*
 * container.h
 *
 *  Created on: Apr 4, 2021
 *      Author: surligas
 */

#ifndef SIGMF_META_CONTAINER_H_
#define SIGMF_META_CONTAINER_H_

#include <string>
#include <nlohmann/json.hpp>
#include <archive.h>
#include <archive_entry.h>

namespace sigmf
{

class meta_container
{
public:
  static const std::string data_extension;
  static const std::string meta_extension;

  meta_container (const std::string &filename);

  ~meta_container ();

  const nlohmann::json&
  json() const;

  const std::string&
  path() const;

private:
  const std::string m_filename;
  bool m_found_meta;
  bool m_found_data;
  nlohmann::json m_json;
};

} /* namespace sigmf */

#endif /* SIGMF_META_CONTAINER_H_ */
