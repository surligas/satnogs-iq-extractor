/*
 * extractor_sigmf_to_hdf5.cpp
 *
 *  Created on: May 26, 2021
 *      Author: surligas
 */

#include "extractor_sigmf_to_hdf5.h"

#include <iostream>
#include <archive.h>
#include <archive_entry.h>
#include <fcntl.h>
#include <exception>
#include <cstring>
#include <fstream>
#include <random>
#include <filesystem>

extractor_sigmf_to_hdf5::extractor_sigmf_to_hdf5 (
    const std::string &in_dir, const std::string &out_dir, size_t block_size,
    size_t skip_start, size_t skip_end, bool awgn, size_t awgn_blocks)
: extractor(in_dir, out_dir, block_size, skip_start, skip_end, awgn, awgn_blocks, ".sigmf"),
  /* FIXME! hardcoded sc16 support detected!!!! */
  m_sizeof_data_sample(2 * sizeof(int16_t)),
  m_max_class_samples(0),
  m_class_samples_cnt(0)
{
}

extractor_sigmf_to_hdf5::~extractor_sigmf_to_hdf5 ()
{
}

void
extractor_sigmf_to_hdf5::print_settings ()
{
  std::cout << "Metadata Engine           : " << "SigMF" << std::endl;
  std::cout << "Output Format             : " << "HDF5" << std::endl;
  extractor::print_settings ();
}

void
extractor_sigmf_to_hdf5::extract ()
{
  std::map<double, std::vector<sigmf::meta_container>> m;
  std::cout << "Scanning for SigMF IQ captures inside the directory "
      << m_input_dir << std::endl;
  for (auto p : std::filesystem::recursive_directory_iterator (m_input_dir)) {
    /* Check for all .tar files containing sigmf metadata*/
    if (p.path ().extension () == ".tar") {
      std::cout << p.path() << "              : ";
      try {
        sigmf::meta_container c(p.path().string());
        m[c.json()["global"]["core:sample_rate"].get<double>()].push_back(c);
        std::cout << "OK" << std::endl;
      }
      catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
      }
    }
  }

  for(auto &i : m) {
    extract_samp_rate_group(i.first, i.second);
  }
}

void
extractor_sigmf_to_hdf5::extract_samp_rate_group (
  double samp_rate, const std::vector<sigmf::meta_container> &archs)
{
  /* Scan the several information needed to build the dataset */
  std::map<std::string, std::vector<sigmf::meta_container>> classes;
  std::map<std::string, size_t> class_samples;

  std::cout << "Generating dataset for the sampling rate  " << samp_rate
            << ": ";

  for(auto &i : archs) {
    const std::string cl = i.json()["global"]["satnogs:transmitter"].get<std::string>();

    if(i.json()["annotations"].size()) {
      classes[cl].push_back(i);
      for(auto &annot : i.json()["annotations"]) {
        class_samples[cl] +=
            (annot["core:sample_count"].get<size_t> ()
                * i.json ()["global"]["satnogs:decoder_resampling_ratio"].get<
                    double> ()) / m_block_size;
      }
    }

    if(m_awgn && i.json()["annotations"].size() == 0) {
        /*
         * Having zero annotations does not mean that there is no signal.
         * Use the user vetting to make sure that this observation can be used
         * for AWGN training
         */
        if(!has_signal(i.json()["global"]["satnogs:observation_id"])) {
           classes["awgn"].push_back(i);
           class_samples["awgn"] = SIZE_MAX;
        }
    }
  }

  /* Consider AWGN only dataset or empty classes an error */
  if((classes.size() <= 1 && m_awgn) || classes.size() == 0) {
    std::cout << "Failed! Insufficient classes on the given dataset." << std::endl;
    return;
  }

  /*
   * To ensure that no class is biased, we produce the same amount of samples
   * for all available classes
   */
  auto m = std::min_element(class_samples.cbegin(), class_samples.cend(),
                            [](const auto& l, const auto& r) { return l.second < r.second; });
  m_max_class_samples = m->second;

  /* Write the available classes in a file */
  std::filesystem::path p_classes = std::filesystem::path(m_output_dir);
  p_classes /= std::to_string(samp_rate) + "_classes.txt";
  std::ofstream cf (p_classes.string(), std::ios::out);
  std::string s = "[";
  for(auto &i : classes) {
    s += i.first + ", ";
  }
  s.erase(s.length() - 2);
  s += "]";
  cf << s;

  std::filesystem::path p(m_output_dir);
  p /= std::to_string(samp_rate) + ".h5";
  h5::fd_t fd = h5::create(p.string(), H5F_ACC_TRUNC);
  h5::pt_t iq = h5::create<float>(fd, "X",
                                   h5::max_dims{H5S_UNLIMITED, m_block_size, 2},
                                   h5::chunk{1, m_block_size, 2} | h5::fill_value<float>(0.0f) );
  h5::pt_t labels = h5::create<int>(fd, "Y",
                                    h5::max_dims{H5S_UNLIMITED, classes.size()},
                                    h5::chunk{1, classes.size()} | h5::fill_value<int>(0) );
  h5::pt_t snr = h5::create<float> (fd, "Z", h5::max_dims{ H5S_UNLIMITED, 1 },
                                  h5::chunk{ 1, 1 } | h5::fill_value<float> (-130.0f));

  /* Start extracting! */
  size_t class_idx = 0;
  for(auto& i : classes) {
      if(i.first == "awgn") {
        extract_awgn_class(class_idx, classes.size(), i.second, iq, labels, snr);
      }
      else {
        extract_class(class_idx, classes.size(), i.second, iq, labels, snr);
      }
      class_idx++;
  }
  std::cout << "Done!" << std::endl;
}

void
extractor_sigmf_to_hdf5::extract_class (size_t class_idx,
                                        size_t nclasses,
                                        const std::vector<sigmf::meta_container>& archs,
                                        h5::pt_t &iq_dataset,
                                        h5::pt_t &class_dataset,
                                        h5::pt_t &snr_dataset)
{
  m_class_samples_cnt = 0;
  for(auto &i : archs) {
    extract_observation(class_idx, nclasses, i, iq_dataset,
                        class_dataset, snr_dataset);
  }
}

void
extractor_sigmf_to_hdf5::extract_awgn_class (size_t class_idx,
                                             size_t nclasses,
                                             const std::vector<sigmf::meta_container>& archs,
                                             h5::pt_t &iq_dataset,
                                             h5::pt_t &class_dataset,
                                             h5::pt_t &snr_dataset)
{
  m_class_samples_cnt = 0;
  size_t blocks = std::ceil (
      static_cast<float> (m_max_class_samples) / archs.size ());
  for(auto &i : archs) {
    extract_awgn_observation(class_idx, nclasses, i, iq_dataset,
                             class_dataset, snr_dataset, blocks);
  }
}

void
extractor_sigmf_to_hdf5::extract_observation (size_t class_idx,
                                              size_t nclasses,
                                              const sigmf::meta_container& c,
                                              h5::pt_t &iq_dataset,
                                              h5::pt_t &class_dataset,
                                              h5::pt_t &snr_dataset)
{
  struct archive *tar_arch = archive_read_new ();

  if (!tar_arch) {
    throw std::runtime_error (
        "Could not allocate reading achieve memory structure");
  }

  if (archive_read_support_format_tar (tar_arch)) {
    throw std::runtime_error ("Could set the tar filter");
  }

  struct archive_entry *entry;
  int r;
  r = archive_read_open_filename (tar_arch, c.path ().c_str (), 10240);
  bool found_data = false;
  while ((r = archive_read_next_header (tar_arch, &entry)) == ARCHIVE_OK) {
    std::string s (archive_entry_pathname (entry));
    found_data = s.size () >= sigmf::meta_container::data_extension.size ()
        && std::equal (sigmf::meta_container::data_extension.rbegin (),
                       sigmf::meta_container::data_extension.rend (),
                       s.rbegin ());
    if (found_data) {
      break;
    }
  }

  if (!found_data) {
    throw std::runtime_error (c.path () + ": invalid SigMF archive");
  }

  uint64_t index = 0;
  const nlohmann::json &j = c.json ();
  double resampling_ratio = j["global"]["satnogs:decoder_resampling_ratio"];
  uint64_t phase = j["global"]["satnogs:decoder_phase"];
  for (auto &elem : j["annotations"]) {
    uint64_t sample_cnt = elem["core:sample_count"];
    uint64_t sample_start = elem["core:sample_start"];
    double timing_error = elem["satnogs:symbol_timing_error"];

    /*
     * Correct the sample start based on the timing error, the phase and the
     * resampling ratio.
     *
     * Note that the the phase offset does not need any kind of correction
     */
    uint64_t true_sample_start = resampling_ratio
        * (sample_start * timing_error) - phase + m_skip_start;
    uint64_t true_sample_cnt = resampling_ratio * (sample_cnt * timing_error)
        - m_skip_end;

    /* Seek until the start of the frame */
    int16_t iq[m_block_size * 2];
    while (index < true_sample_start * m_sizeof_data_sample) {
      uint64_t block = std::min (true_sample_start * m_sizeof_data_sample,
                                 index + m_block_size * m_sizeof_data_sample)
          - index;

      ssize_t ret = archive_read_data (tar_arch, iq, block);
      if (ret <= 0) {
        archive_read_close (tar_arch);
        archive_read_free (tar_arch);
        throw std::runtime_error ("unexpected end of file");
      }
      index += ret;
    }

    for (uint64_t i = 0; i < true_sample_cnt / m_block_size; i++) {
      if(m_class_samples_cnt == m_max_class_samples) {
        archive_read_close (tar_arch);
        archive_read_free (tar_arch);
        return;
      }
      ssize_t ret = archive_read_data (tar_arch, iq,
                                       m_block_size * m_sizeof_data_sample);
      if (ret != m_block_size * m_sizeof_data_sample) {
        throw std::runtime_error ("could not read the specified block size");
      }

      /* Convert to float and append to the dataset */
      std::vector<float> d(m_block_size * 2);
      std::transform(iq, iq + m_block_size * 2, d.begin(), [](int16_t x) { return (float) x;});
      std::vector<int> labels (nclasses);
      std::fill(labels.begin(), labels.end(), 0);
      labels[class_idx] = 1;

      h5::append(iq_dataset, d);
      h5::append(class_dataset, labels);
      h5::append(snr_dataset, -130.0f);

      index += m_block_size * m_sizeof_data_sample;
      m_class_samples_cnt++;
    }
  }
  archive_read_close (tar_arch);
  archive_read_free (tar_arch);
}

void
extractor_sigmf_to_hdf5::extract_awgn_observation (size_t class_idx,
                                                   size_t nclasses,
                                                   const sigmf::meta_container& c,
                                                   h5::pt_t &iq_dataset,
                                                   h5::pt_t &class_dataset,
                                                   h5::pt_t &snr_dataset,
                                                   size_t nblocks)
{
  struct archive *tar_arch = archive_read_new ();
  if (!tar_arch) {
    throw std::runtime_error (
        "Could not allocate reading achieve memory structure");
  }
  if (archive_read_support_format_tar (tar_arch)) {
    throw std::runtime_error ("Could set the tar filter");
  }
  struct archive_entry *entry;
  int r;
  r = archive_read_open_filename (tar_arch, c.path ().c_str (), 10240);
  bool found_data = false;

  size_t file_size = 0;
  while ((r = archive_read_next_header (tar_arch, &entry)) == ARCHIVE_OK) {
    std::string s (archive_entry_pathname (entry));
    found_data = s.size () >= sigmf::meta_container::data_extension.size ()
        && std::equal (sigmf::meta_container::data_extension.rbegin (),
                       sigmf::meta_container::data_extension.rend (),
                       s.rbegin ());
    la_int64_t fs = archive_entry_size(entry);
    if(fs <= 0) {
      found_data = false;
      break;
    }
    file_size = fs;

    if (found_data) {
      break;
    }
  }

  if (!found_data) {
    throw std::runtime_error (c.path () + ": invalid SigMF archive");
  }

  /* Try to randomize as much as possible the sampling of the AWGN blocks */
  const size_t gap = file_size / nblocks;
  uint64_t index = 0;
  for(size_t i = 0; i < nblocks; i++) {
    if(m_class_samples_cnt >= m_max_class_samples) {
      archive_read_close (tar_arch);
      archive_read_free (tar_arch);
      return;
    }
    std::mt19937 mt;
    auto rng = std::uniform_int_distribution<size_t> (i*gap,
        (i + 1) * gap - m_block_size * m_sizeof_data_sample * 2);

    size_t offset = rng(mt);
    int16_t iq[m_block_size * 2];
    /* Seek until the start of the frame */
    while (index <= offset) {
      ssize_t ret = archive_read_data (tar_arch, iq,
                                       m_block_size * m_sizeof_data_sample);
      if (ret <= 0) {
        archive_read_close (tar_arch);
        archive_read_free (tar_arch);
        throw std::runtime_error ("unexpected end of file");
      }
      index += ret;
    }
    m_class_samples_cnt++;

    std::vector<float> d (m_block_size * 2);
    std::vector<int> labels (nclasses);
    std::fill(labels.begin(), labels.end(), 0);
    labels[class_idx] = 1;
    std::transform (iq, iq + m_block_size * 2,
                    d.begin (), [](int16_t x) {return (float) x;});
    h5::append (iq_dataset, d);
    h5::append (class_dataset, labels);
    h5::append (snr_dataset, -130.0f);
  }
  archive_read_close (tar_arch);
  archive_read_free (tar_arch);
}

extractor::sptr
extractor_sigmf_to_hdf5::make_shared (const std::string &in_dir,
                                      const std::string &out_dir,
                                      size_t block_size, size_t skip_start,
                                      size_t skip_end, bool awgn,
                                      size_t awgn_blocks)
{
  return extractor::sptr (
      new extractor_sigmf_to_hdf5 (in_dir, out_dir, block_size, skip_start,
                                   skip_end, awgn, awgn_blocks));
}
