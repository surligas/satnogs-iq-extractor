# satnogs-iq-extractor: Skyrocket your ML project through SatNOGS!
This tools is suitable for creating databases that can be used by 
Machine Learning systems, utilizing raw IQ samples from the SatNOGS network/

Currently only the SigMF metadata specification is supported, but the architecture
of the project is easily expandable to other formats, eg HDF5.

## Requirements
* CMake
* G++ (C++17 support)
* cpprest
* nlohmann JSON
* libarchive 

## Build from source
* `git clone https://gitlab.com/surligas/satnogs-iq-extractor.git`
* `cd satnogs-iq-extractor.git`
* `mkdir build`
* `cd build`
* `cmake ..`
* `make`

The executable can be found in the `build/src` directory.

## Usage
Use the `satnogs-iq-extractor --help` for a detailed description of the available
parameters.